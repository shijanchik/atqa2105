
package cycletask22;
public class CycleTask22 {

    public static void main(String[] args) {
        boolean isset=false;
        int n=862;
        while (n>0) {
        
            if (n%10%2!=0) {
               isset=true;             
            }
            n=n/10;
        }
        if (isset) {
            System.out.println("True");
        } else {
            System.out.println("False");
        }
    }
    
}
