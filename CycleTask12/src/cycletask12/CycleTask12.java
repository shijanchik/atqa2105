
package cycletask12;
public class CycleTask12 {

    public static void main(String[] args) {
        int n=4;
        double mult=1;
        for (int i=1; i<=n; i++){
            System.out.println(i);
            mult=mult*i;
        }
        System.out.println("Факториал числа равен "+mult);
    }
    
}
