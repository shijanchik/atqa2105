
package animalexample;

public class Employee extends Person {
    private String position;
    private int code;

    @Override
    public String toString() {
        return "Employee{" + "position=" + position + ", code=" + code + '}'
                +super.toString();
    }

    public Employee(String position, int code, String fname, String lname, int sex, int age, String email, String phone) {
        super(fname, lname, sex, age, email, phone);
        this.position = position;
        this.code = code;
    }


    }

