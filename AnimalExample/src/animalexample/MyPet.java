
package animalexample;

public class MyPet extends Animal{
    private String nickname;

    @Override
    public String toString() {
        
        String result=this.nickname+", "
                + "sex="+this.getSex()+", "
                + "age (months)= " 
                +this.getAge();
        return result;
    
    }
    MyPet(String nick, int sex, int age) {
        super(sex, age);//передали данные родителю
        this.nickname=nick;
    
    }
}
