
package animalexample;

public class Man extends Animal{
   private String firstName;
   private String lastName;

    @Override
    public String toString() {
        return " firstName = " + firstName + " lastName = " 
                + lastName +" age(year) "+this.getAge()/12+ 
                " sex = "+this.getSex();
    }
   Man(String fName, String lName, int sex, int age){
   
   super(sex,age);
   this.firstName=fName;
   this.lastName=lName;
   
   }
}
