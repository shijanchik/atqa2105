
package animalexample;

public class Animal {
    int sex;
    private int age;

    public int getSex() {
        return sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "Animal{" + "sex=" + sex + ", age=" + age + '}';
    }

    public Animal(int sex, int age) {
        this.sex = sex;
        this.age = age;
    }
}
