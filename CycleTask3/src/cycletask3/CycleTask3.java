
package cycletask3;
public class CycleTask3 {

    public static void main(String[] args) {
        int x = 265;
        System.out.println("число = " + x);
        int counter = 0;
        int sumDigits = 0;
        while (x != 0) {
            sumDigits = sumDigits + x%10;
            x = x/10;
            counter = counter + 1;
            
        }
        System.out.println("Количество цифр в нем " + counter);
        System.out.println("Сумма цифр = " + sumDigits);
    }
    
}
