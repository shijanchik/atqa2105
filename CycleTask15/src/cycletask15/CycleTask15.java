
package cycletask15;
public class CycleTask15 {

    public static void main(String[] args) {
        int n=3;
        int sum=0;
        int degree=0;
        for (int i=1; i<=n; i++) {
        degree=degr(i);
        sum=sum+degree;
        }
        System.out.println(sum);
        
    }
    public static int degr(int a) {
    int sum=1;
    for (int i=1; i<=a; i++) {
        sum=sum*a;
    }
    return sum;
    
    
    }
    
}
