
package employeeexample;

public class Programmer extends AbstractEmployee implements IEmployee {

    @Override
    public String toString() {
        return "Programmer{" + "base=" + this.getBase() + '}';
    }

    public Programmer(double salary) {
        super(salary);
     
    } 
    
    @Override
    public double calcSalary() {   
        return this.getBase()*8;
    }
}
