
package employeeexample;

public interface IEmployee {
    double calcSalary();
    String getTitle();
}
