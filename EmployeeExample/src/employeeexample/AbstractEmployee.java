
package employeeexample;

public class AbstractEmployee implements IEmployee {
    private double base;
    
    AbstractEmployee (double salary) {
        this.base=salary;
    }
    
    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }
    
    @Override
    public String getTitle () {
    return "";
    }
    
    @Override
    public double calcSalary() {
        return this.base;
    }
    
}
