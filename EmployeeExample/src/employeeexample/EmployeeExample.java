
package employeeexample;

public class EmployeeExample {

    public static void main(String[] args) {
       Cleaner cl=new Cleaner (3400);
       double clSalary = cl.calcSalary();
       String clTitle = cl.getTitle();
       System.out.println(cl.toString());
       System.out.println("Уборщик "+clTitle+" получает "+clSalary+" грн. в месяц");
       
       Accountant ac=new Accountant (6500);
       double acSalary = ac.calcSalary();
       String acTitle = ac.getTitle();
      
       System.out.println("Бухгалтер "+acTitle+" получает "+acSalary+" грн. в месяц");
       
       Programmer pr=new Programmer (200);
       double prSalary = pr.calcSalary();
       String prTitle = pr.getTitle();
      
       System.out.println("Программист "+prTitle+" получает "+prSalary+" грн. в день");
       
       Leader le=new Leader (15500);
       double leSalary = le.calcSalary();
       String leTitle = le.getTitle();
      
       System.out.println("Руководитель "+leTitle+" получает "+leSalary+" грн. в месяц");
    }
    
}
