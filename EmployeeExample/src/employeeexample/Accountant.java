
package employeeexample;

public class Accountant extends AbstractEmployee implements IEmployee {

    @Override
    public String toString() {
        return "Accountant{" + "base=" + this.getBase() + '}';
    }

    public Accountant(double salary) {
        super(salary);
    }    
    
}

