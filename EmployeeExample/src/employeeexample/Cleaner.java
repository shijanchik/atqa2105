

package employeeexample;

public class Cleaner extends AbstractEmployee implements IEmployee {

    @Override
    public String toString() {
        return "Зарплата уборщика = " + this.getBase()+" в месяц";
    }

    public Cleaner(double salary) {
        super(salary);
    }
    
}
