
package employeeexample;

public class Leader extends AbstractEmployee implements IEmployee {
    
    @Override
    public String toString() {
        return "Leader{" + "base=" + this.getBase() + '}';
    }

    public Leader(double salary) {
        super(salary);
    }
    
     @Override
    public double calcSalary() {   
        return this.getBase()+0.2*this.getBase();
    }
}
